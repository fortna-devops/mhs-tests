#! /usr/bin/env python3

import logging
import requests
import boto3
import json
from analytics_sdk_core.data_init import data_pull
from analytics_sdk_core.constants import card_colors
from analytics_sdk_core.constants import equipment_codes


# Configs


AUTH_REQUEST_URL = 'https://mhsinsights.auth.us-east-1.amazoncognito.com/oauth2/authorize'
AUTH_REQUEST_DATA = {
    'client_id': '5duuv4418bja5en9dd39kdk6ms',
    'response_type': 'code',
    'redirect_uri': 'https://mhsinsights.com/'
}
AUTH_DATA = {
    'username': 'user_for_testsuite',
    'password': 's+Uw<x9NdV;ZWZp,G6)bj<sw&L]r7S`e'
}

SITES = [
    {
        'name': 'fedex-louisville',
        'url': 'https://fvflqmj6ab.execute-api.us-east-1.amazonaws.com/prod/location?site=fedex-louisville'
    }, {
        'name': 'dhl-miami',
        'url': 'https://fvflqmj6ab.execute-api.us-east-1.amazonaws.com/prod/location?site=dhl-miami'
    }
]

PROFILE_NAME = 'MHS'
BUCKET_NAME_PREFIX = 'mhspredict-site-'


# Functions


def to_str(d):
    return ', '.join("%s: %s" % (key.upper(), value) for key, value in d.items())


def assert_value(key, site_name, d):
    value = d.get(key)
    assert value, '"%s" not found for: "%s" in %s' % (key, site_name, to_str(d))


def bucket_name(site_name):
    return '%s%s' % (BUCKET_NAME_PREFIX, site_name)


def get_locations(engine, site_name):
    query = 'SELECT * FROM "%s"."location"' % bucket_name(site_name)
    return engine.execute_generic_query(query, return_as_df=False)


def get_stat(engine, sensor, site_name):
    return engine.execute_stats_table_query(sensor_name=sensor, gateway_name=site_name)[0]


def skip_sensor(location):
    equipment = location['equipment']
    if equipment.upper() == equipment_codes.STRUCTURE.upper():
        return True
    if equipment.upper() == 'PLC':
        return True
    return False


def compare_thresholds(values, thresholds):
    colors = {}
    for key, value in values.items():
        red_threshold = float(thresholds[key]['red'])
        yellow_threshold = float(thresholds[key]['orange'])
        if key not in thresholds.keys():
            colors[value] = card_colors.GRAY
            continue
        if value >= red_threshold:
            colors[value] = card_colors.RED
            continue
        if value <= yellow_threshold:
            colors[value] = card_colors.GREEN
            continue
        if yellow_threshold < value < red_threshold:
            colors[value] = card_colors.YELLOW
            continue
    return colors


def calculate_color(location, stat):
    equipment = location['equipment']
    values = {
        'temperature': stat['temperature_perc95'],
        'rms_velocity_x': stat['rms_velocity_x_perc95'],
        'rms_velocity_z': stat['rms_velocity_z_mean']
    }
    if equipment.upper() == equipment_codes.BEARING.upper():
        values['hf_rms_acceleration_x'] = stat['hf_rms_acceleration_x_perc95']
        values['hf_rms_acceleration_z'] = stat['hf_rms_acceleration_z_perc95']
    values = {key: float(value) for key, value in values.items()}
    thresholds = json.loads(location['thresholds'])
    colors = compare_thresholds(values, thresholds)
    return max(colors.values())


def get_sensor_locations(profile_name, site_name):
    session = boto3.Session(profile_name=profile_name)
    engine = data_pull.DataQueryEngineLite(bucket_name(site_name), boto_session=session)
    logging.info('get locations for: %s' % site_name)
    locations = get_locations(engine, site_name)
    sensor_locations = {}
    for location in locations:
        assert_value('sensor', site_name, location)
        assert_value('equipment', site_name, location)
        if skip_sensor(location):
            continue
        sensor = location['sensor']
        logging.info('get stat for: %s, sensor: %s' % (site_name, sensor))
        assert_value('thresholds', site_name, location)
        stat = get_stat(engine, sensor, site_name)
        location['color'] = calculate_color(location, stat)
        sensor_locations[sensor] = location
    return sensor_locations


def color_to_str(color):
    if color == card_colors.RED:
        return 'RED'
    if color == card_colors.YELLOW:
        return 'YELLOW'
    if color == card_colors.GREEN:
        return 'GREEN'
    if color == card_colors.GRAY:
        return 'GRAY'
    return 'UNKNOWN_COLOR'


def compare_colors(sensor_locations, locations, site_name):
    num_mismatches = 0
    for location in locations:
        assert_value('sensor', site_name, location)
        sensor = location['sensor']
        if skip_sensor(location):
            continue
        sensor_location = sensor_locations.get(sensor)
        if not sensor_location:
            logging.warning('site: "%s", sensor "%s" not found in locations' % (site_name, sensor))
            num_mismatches += 1
            continue
        location['standards_score'] = int(location['standards_score'])
        if location['standards_score'] != sensor_location['color']:
            num_mismatches += 1
            color = color_to_str(location['standards_score'])
            expected_color = color_to_str(sensor_location['color'])
            logging.warning('site: "%s", conveyor: "%s", sensor: "%s", color: "%s", expected_color: "%s"' %
                            (site_name, location['conveyor'], sensor, color, expected_color))
    logging.warning('site: "%s", %s locations analyzed, %s mismatches found' %
                    (site_name, len(sensor_locations), num_mismatches))


# Test


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info('get token from: %s' % AUTH_REQUEST_URL)
    session = requests.Session()
    response = session.get(AUTH_REQUEST_URL, params=AUTH_REQUEST_DATA)
    response.raise_for_status()
    logging.info('post auth data to: %s' % response.url)
    AUTH_DATA['_csrf'] = response.history[-1].headers['set-cookie'].lstrip('XSRF-TOKEN=').split(';', 1)[0]
    response = session.post(response.url, data=AUTH_DATA)
    response.raise_for_status()
    session.headers.update({'Authorization': 'Bearer %s' % session.cookies['token']})
    for site in SITES:
        try:
            sensor_locations = get_sensor_locations(PROFILE_NAME, site['name'])
            logging.info('get data from: %s' % site['url'])
            response = session.get(site['url'])
            response.raise_for_status()
            locations = response.json().get('data')
            assert locations, '"data" not found for: "%s"' % site['name']
            compare_colors(sensor_locations, locations, site['name'])
        except AssertionError as error:
            logging.error(error)


if __name__ == '__main__':
    main()
